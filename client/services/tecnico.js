var angular = require('angular');

angular.module('makicop')
.service('TecnicoService',['$http', function($http){
  var urlBase = url_base + '/tecnico';

  this.getMisMaquinas = function(){
    return $http.get(urlBase + '/get-mis-maquinas');
  }

  this.getOnReports = function () {
    return $http.get(urlBase + '/get-on-reports');
  }

  this.finishReportOk = function (d) {
    return $http.post(urlBase + '/finish-report-ok', d);
  }

  this.getReporteSearh = function (d) {
    return $http.post(urlBase + '/get-report-search', d);
  }

}]);
