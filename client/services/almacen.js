var angular = require('angular');

angular.module('makicop')
.service('AlmacenService',['$http', function($http){
  var urlBase = url_base + '/almacen';

  this.searchSerie = function(d){
    return $http.post(urlBase + '/search-serie', d);
  }

  this.insertMachine = function(d){
    return $http.post(urlBase + '/insert-machine', d);
  }

  this.buscarMaquina = function(d){
    return $http.post(urlBase + '/buscar-maquina', d);
  }

  this.updateInfoMaquina = function(d){
    return $http.post(urlBase + '/update-maquina', d);
  }

  this.getMaquinas = function(){
    return $http.get(urlBase + '/get-maquina');
  }



}]);
