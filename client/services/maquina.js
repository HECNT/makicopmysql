var angular = require('angular');

angular.module('makicop')
.service('MaquinaService',['$http', function($http){
  var urlBase = url_base + '/maquina';

  this.getMaquina = function(){
    return $http.get(urlBase + '/get-maquina');
  }

  this.doReport = function(d){
    console.log(d);
    return $http.post(urlBase + '/do-report', d);
  }

  this.getBitacora = function(d){
    console.log(d);
    return $http.post(urlBase + '/get-bitacora', d);
  }

}]);
