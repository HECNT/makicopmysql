var angular = require('angular');
url_base = 'http://localhost:3000';

angular.module('makicop', ['angularUtils.directives.dirPagination'])
.config(['$interpolateProvider', function($interpolateProvider){
  $interpolateProvider.startSymbol('{[');
  $interpolateProvider.endSymbol(']}');
}]);
$ = require('jquery');
//Login
require('./controllers/login');

//require('./controllers/main');
require('./controllers/maquina');
require('./controllers/almacen');
require('./controllers/tecnico');
//require('./controllers/fuentes');
//require('./controllers/detalles');
