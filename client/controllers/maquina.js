var angular = require('angular');

require('../services/maquina');

angular.module('makicop')
.controller('maquinaCtrl',['$scope', 'MaquinaService', function($scope, MaquinaService){


  /*MaquinaService.getMaquina()
  .then(function(result){
    $scope.maquinas = result.data;
    console.log(result);
  });*/

  $scope.asignar = function(item){
    $scope.show_form_reporte = true;
    $scope.show_form_reporte_ok = false;
    console.log(item);
    $scope.id_maquinaG = item.id_maquina;
    $scope.marca = item.serie;
    $scope.Modelo = item.modelo;
    $scope.Marca = item.marca;
    $scope.Contador_n = item.contador_negro;
    $scope.Contador_c = item.contador_color;
    $scope.Agregados = item.agregados;
    $scope.Cliente = item.cliente;
    $scope.Tecnico = item.tecnico;
    $scope.reporta_cliente = item.contacto;
  }
  $scope.show_form_reporte = true;
  $scope.show_form_reporte_ok = false;
  $scope.doReport = function (it, ite) {
    var objReport = {reporte_falla:it,reporta:ite, id_maquina:$scope.id_maquinaG,tecnico:$scope.Tecnico, serie: $scope.marca}
    console.log(objReport);
    MaquinaService.doReport(objReport)
    .then(function(result){
      if (result.statusText == "OK") {
        $scope.show_form_reporte = false;
        $scope.show_form_reporte_ok = true;
        $scope.folio = result.data.insertId;
      }
    });
  }

  $scope.getBitacora = function (item) {
    console.log(item);
    MaquinaService.getBitacora(item)
    .then(function(result){
      if (result.statusText == "OK") {
        console.log(result.data);
        $scope.bitacora = result.data;
      }
    });
  }


}]);
