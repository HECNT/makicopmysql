var angular = require('angular');
var Chart = require('chart.js');

require('../services/almacen');

angular.module('makicop')
.controller('almacenCtrl',['$scope', 'AlmacenService', function($scope, AlmacenService){


//PARA VALIDAR SERIE
$scope.show_err_serie = false;
$scope.show_rest_form = true;
  $scope.searchSerie = function (item) {
    var d = {serie:item};
    console.log(d);
    AlmacenService.searchSerie(d)
    .then(function(result){
      console.log(result);
      if (result.data.length == 0) {
        $scope.show_rest_form = true;
        $scope.show_err_serie = false;
        console.log('No se encontro serie');
      } else {
        $scope.show_rest_form = false;
        $scope.show_err_serie = true;
      }
    });
  }
//INSERTA NUEVA MAQUINA
$scope.btnMostartModalMaquina = function (){
  $scope.show_gnral_form_maquina = true;
  $scope.show_exito_maquina = false;
  $scope.agregarM.serie = "";
  $scope.agregarM.modelo = "";
  $scope.agregarM.marca = "";
  $scope.agregarM.negro = "";
  $scope.agregarM.color = "";
  $scope.agregarM.agregados = "";
  $scope.agregarM.cliente = "";
  $scope.agregarM.tecnico = "";
  $scope.agregarM.calle = "";
  $scope.agregarM.colonia = "";
  $scope.agregarM.municipio = "";
  $scope.agregarM.estado = "";
  $scope.agregarM.pais = "";
  $scope.agregarM.cp = "";
  $scope.agregarM.contacto = "";
  $scope.agregarM.telefono = "";
  $scope.agregarM.departamento = "";
  $scope.agregarM.subcliente = "";
}
$scope.show_exito_maquina = false;
$scope.show_gnral_form_maquina = true;
  $scope.insertMachine = function (item) {
    if ($scope.revisar($scope.agregarM, $scope.agregarMVal, $scope.agregarM_err)==0) {
      console.log(item);
      AlmacenService.insertMachine(item)
      .then(function(result){
        console.log(result);
        $scope.idG = result.data.insertId;
        if (result.statusText == "OK") {
          $scope.show_exito_maquina = true;
          $scope.show_gnral_form_maquina = false;
        }
      });
    }
  }

//PARA BUSCAR MAQUINA
$scope.show_radio_id = false;
$scope.show_radio_serie = false;
$scope.show_radio_modelo = false;
$scope.radioId = function() {
  $scope.show_radio_id = true;
  $scope.show_radio_serie = false;
  $scope.show_radio_modelo = false;
}

$scope.radioSerie = function() {
  $scope.show_radio_id = false;
  $scope.show_radio_serie = true;
  $scope.show_radio_modelo = false;
}

$scope.radioModelo = function() {
  $scope.show_radio_id = false;
  $scope.show_radio_serie = false;
  $scope.show_radio_modelo = true;
}

$scope.show_tabla_busqueda_maquina= false;
$scope.show_exito__busqueda_maquina = false;
$scope.buscarId = function (item) {
//console.log(item);
item.tipo = "id";
  AlmacenService.buscarMaquina(item)
  .then(function(result){
    console.log(result);
    $scope.maquinaE = result.data;
    if (result.data.length == 0) {
      $scope.show_tabla_busqueda_maquina= false;
      $scope.show_exito__busqueda_maquina = true
    }else {
      $scope.show_tabla_busqueda_maquina= true;
      $scope.show_exito__busqueda_maquina = false;
    }
  });
}

$scope.buscarSerie = function (item) {
//console.log(item);
item.tipo = "serie";
AlmacenService.buscarMaquina(item)
.then(function(result){
  console.log(result);
  $scope.maquinaE = result.data;
  if (result.data.length == 0) {
    $scope.show_tabla_busqueda_maquina= false;
    $scope.show_exito__busqueda_maquina = true;
  }else {
    $scope.show_tabla_busqueda_maquina= true;
    $scope.show_exito__busqueda_maquina = false;
  }
});
}

$scope.buscarModelo = function (item) {
//console.log(item);
item.tipo = "modelo";
AlmacenService.buscarMaquina(item)
.then(function(result){
  console.log(result);
  $scope.maquinaE = result.data;
  if (result.data.length == 0) {
    $scope.show_tabla_busqueda_maquina= false;
    $scope.show_exito__busqueda_maquina = true;
  }else {
    $scope.show_tabla_busqueda_maquina= true;
    $scope.show_exito__busqueda_maquina = false;
  }
});
}

$scope.verMasInfo = function (item) {
  console.log(item);
  $scope.bSerie         = item.serie;
  $scope.bMarca         = item.marca;
  $scope.bModelo        = item.modelo;
  $scope.bAgregados     = item.agregados;
  $scope.bCalle         = item.calle;
  $scope.bCliente       = item.cliente;
  $scope.bColonia       = item.colonia;
  $scope.bContacto      = item.contacto;
  $scope.bNegro         = item.contador_negro;
  $scope.bColor         = item.contador_color;
  $scope.bCp            = item.cp;
  $scope.bDepartamento  = item.departamento;
  $scope.bEstado        = item.estado;
  $scope.bEstatus       = item.estatus;
  $scope.bMunicipio     = item.municipio;
  $scope.bPais          = item.pais;
  $scope.bSubCliente    = item.sub_cliente;
  $scope.bTecnico       = item.tecnico;
  $scope.bTelefono      = item.telefono;
}

$scope.verMasInfoEdit = function (item) {
  $scope.show_alert_update_maquina = false;
  $scope.show_ok_update_maquina = false;
  //console.log(item,'verMas info');
  //$scope.bSerie = "Hola"
  $scope.edit = {bSerie: "",
  bMarca:"",
  bModelo:"",
  bAgregados:"",
  bCalle:"",
  bCliente:"",
  bColonia:"",
  bContacto:"",
  bNegro:"",
  bColor:"",
  bCp:"",
  bDepartamento:"",
  bEstado:"",
  bEstatus:"",
  bMunicipio:"",
  bPais:"",
  bSubCliente:"",
  bTecnico:"",
  bTelefono:""
}
  $scope.edit.bIdMaquina     = item.id_maquina;
  $scope.edit.bSerie         = item.serie;
  $scope.edit.bMarca         = item.marca;
  $scope.edit.bModelo        = item.modelo;
  $scope.edit.bAgregados     = item.agregados;
  $scope.edit.bCalle         = item.calle;
  $scope.edit.bCliente       = item.cliente;
  $scope.edit.bColonia       = item.colonia;
  $scope.edit.bContacto      = item.contacto;
  $scope.edit.bNegro         = item.contador_negro;
  $scope.edit.bColor         = item.contador_color;
  $scope.edit.bCp            = item.cp;
  $scope.edit.bDepartamento  = item.departamento;
  $scope.edit.bEstado        = item.estado;
  $scope.edit.bEstatus       = item.estatus;
  $scope.edit.bMunicipio     = item.municipio;
  $scope.edit.bPais          = item.pais;
  $scope.edit.bSubCliente    = item.sub_cliente;
  $scope.edit.bTecnico       = item.tecnico;
  $scope.edit.bTelefono      = item.telefono;
}
$scope.show_alerta_maquina = true;
$scope.show_ok_update_maquina = false;
$scope.show_alert_update_maquina = false;
$scope.updateInfoMaquina = function (item) {
  console.log(item);
  AlmacenService.updateInfoMaquina(item)
  .then(function(result){
    if (result.data.changedRows > 0) {
      $scope.show_alerta_maquina = false;
      $scope.show_ok_update_maquina = true;
      $scope.show_alert_update_maquina = false;
    } else {
      $scope.show_alerta_maquina = false;
      $scope.show_ok_update_maquina = false;
      $scope.show_alert_update_maquina = true;
    }
    console.log(result,'aqui actualizacion maquina');
  });
}


$scope.aceptarOkUpdate = function () {
  $scope.show_tabla_busqueda_maquina = false;
}

$scope.btnAlertMaquina = function () {
  $scope.show_alerta_maquina = true;
}

//AL INICIAR LA PAGINA ALMACEN

AlmacenService.getMaquinas()
.then(function(result){
  console.log(result);
  $scope.getMa = result.data;
  var ctx = document.getElementById("myChart").getContext('2d');
  var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: ["M", "T", "W", "T", "F", "S", "S"],
      datasets: [{
        backgroundColor: [
          "#2ecc71",
          "#3498db",
          "#95a5a6",
          "#9b59b6",
          "#f1c40f",
          "#e74c3c",
          "#34495e"
        ],
        data: [12, 19, 3, 17, 28, 24, 7]
      }]
    }
  });
});

//FUNCION PARA REVISAR CAMPO
  $scope.agregarM = {};
    $scope.agregarM_err = {};

    $scope.agregarMVal = [
      {nombre:"serie" ,           requerido: 1 , tipo_dato:"text",    long_min:1 ,long_max:50},
      {nombre:"modelo" ,          requerido: 1 , tipo_dato:"text",    long_min:1 ,long_max:50},
      {nombre:"marca",            requerido: 1 , tipo_dato:"text",    long_min:1 ,long_max:50},
      {nombre:"negro" ,           requerido: 1 , tipo_dato:"number",    long_min:1 ,long_max:50},
      {nombre:"color" ,           requerido: 1 , tipo_dato:"number",    long_min:1 ,long_max:50},
      {nombre:"agregados",        requerido: 1 , tipo_dato:"text",    long_min:1 ,long_max:50},
      {nombre:"cliente" ,         requerido: 1 , tipo_dato:"number",    long_min:1 ,long_max:50},
      {nombre:"tecnico" ,         requerido: 1 , tipo_dato:"number",  long_min:1 ,long_max:50},
      {nombre:"calle",            requerido: 1 , tipo_dato:"text",    long_min:1 ,long_max:50},
      {nombre:"colonia" ,         requerido: 1 , tipo_dato:"text",    long_min:1 ,long_max:50},
      {nombre:"municipio" ,       requerido: 1 , tipo_dato:"text",    long_min:1 ,long_max:50},
      {nombre:"estado",           requerido: 1 , tipo_dato:"text",    long_min:1 ,long_max:50},
      {nombre:"pais" ,            requerido: 1 , tipo_dato:"text",    long_min:1 ,long_max:50},
      {nombre:"cp" ,              requerido: 1 , tipo_dato:"number",  long_min:1 ,long_max:50},
      {nombre:"contacto",         requerido: 1 , tipo_dato:"text",    long_min:1 ,long_max:50},
      {nombre:"telefono" ,        requerido: 1 , tipo_dato:"number",  long_min:1 ,long_max:50},
      {nombre:"departamento" ,    requerido: 1 , tipo_dato:"text",    long_min:1 ,long_max:50},
      {nombre:"subcliente",       requerido: 1 , tipo_dato:"text",    long_min:1 ,long_max:50}
    ];


    //validar general cliente
    $scope.revisar=function(formulario,validacion,error){
      var errores = 0;
      validacion.forEach(function(e){
        var resultado = $scope.validar(formulario,e.nombre,e.requerido,e.tipo_dato,e.long_min,e.long_max);
        if(resultado.err){
          errores++;
          error[e.nombre]=resultado.description;
        }else{
          error[e.nombre]=null;
        }
      });
      console.log(errores);
      return errores;
    };

    $scope.validar = function (formulario,nombre,requerido,tipo_dato,long_min,long_max){
      var msg_error = "";
      if(requerido == 1){
        if(formulario[nombre]==undefined){
          msg_error += "Campo Obligatorio ";
        }
      }
      if(formulario[nombre]!=undefined){
        if(tipo_dato=="number"){
          if(isNaN(formulario[nombre]))
          msg_error+="Campo debe ser numerico ";
        }
        /*if(tipo_dato=="date"){
          var r = moment(formulario[nombre]);
          formulario[nombre]=(r.format('YYYY-MM-DD'))
          if(!r._pf.invalidFormat)
          msg_error+="Fecha invalida ";
        }*/
        if(tipo_dato=="email"){
          var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          if(!re.test(formulario[nombre])){
            msg_error+="Formato de correo invalido ";
          }
        }
        if(long_min > -1 || long_max > -1){
          if(long_min > -1 && formulario[nombre].length < long_min){
            msg_error+="La longitud minima es de: " + long_min;
          }
          if(long_max > -1 && formulario[nombre].length > long_max){
            msg_error+="La longitud maxima es de: " + long_max;
          }
        }
      }
      if(msg_error.length>0){

        console.log(nombre);
      }
      return {err:msg_error.length>0,description:msg_error};
    };

    $scope.revisarCampo=function(formulario,validacion,error,nombre){
      var errores = 0;
      validacion.forEach(function(e){
        if(nombre==e.nombre){
          var resultado = $scope.validar(formulario,e.nombre,e.requerido,e.tipo_dato,e.long_min,e.long_max);
          if(resultado.err){
            errores++;
            error[e.nombre]=resultado.description;
          }else{
            error[e.nombre]=null;
          }
        }
      });
    };

}]);
