var angular = require('angular');

require('../services/tecnico');

angular.module('makicop')
.controller('tecnicoCtrl',['$scope', 'TecnicoService', function($scope, TecnicoService){


//TRAER MAQUINAS ASIGNAS PARA EL TECNICO
  $scope.btnVerMisMaquinas = function () {
    TecnicoService.getMisMaquinas()
    .then(function(result){
      $scope.resGetMaquinas = result.data;
    });
  }

  $scope.show_mas_detalles_maquinas = false;
  $scope.btnVerMas = function (item) {
    $scope.show_mas_detalles_maquinas = true;
    $scope.mMarca = item.marca;
    $scope.mModelo = item.modelo;
    $scope.mNegro = item.contador_negro;
    $scope.mColor = item.contador_color;
    $scope.mAgregados = item.agregados;
    $scope.mCliente = item.cliente;
    $scope.mCp = item.cp;
    $scope.mContacto = item.contacto;
    $scope.mSubC = item.sub_cliente;
    $scope.mTelefono = item.telefono;
  }

//TRAER REPORTES ACTIVOS TECNICO
  TecnicoService.getOnReports()
  .then(function(result){
    //console.log(result,'reportes activos');
    $scope.reportesActivos = result.data;
    $scope.countResultReports = result.data.length;
    console.log(result.data.length,'conteo');
  });

//VER DETALLES REPORTE MAQUINA
  $scope.btnDetallesTecnico = function (item) {
    console.log(item,'reporte detalles');
    $scope.rSerie = item.serie;
    $scope.rMarca = item.marca;
    $scope.rModelo = item.modelo;
    $scope.rNegro = item.contador_negro;
    $scope.rColor = item.contador_color;
    $scope.rAgregados = item.agregados;
    $scope.rCliente = item.cliente;
    $scope.rCp = item.cp;
    $scope.rContacto = item.contacto;
    $scope.rSubC = item.sub_cliente;
    $scope.rTelefono = item.telefono;
    $scope.fFallaReportada = item.falla_reportada;
    $scope.rFecha = item.fecha_captura;
    $scope.rReporta = item.cliente_captura;
    $scope.rId_reporte = item.id_reporte;
  }

  //PARA FINALIZAR REPORTE
  $scope.show_si_reporte = false;
  $scope.show_si_no_reporte = false;
  $scope.show_reporte_detalle_ver = false;
  $scope.show_reporte_detalle = true;
  $scope.show_si_reporte_ok = false;
    $scope.btnFinalReporte = function () {
      $scope.show_si_reporte = false;
      $scope.show_si_no_reporte = true;
      $scope.show_reporte_detalle = false;
      $scope.show_reporte_detalle_ver = true;
    }

  $scope.btnSiFinalReporteMaquina = function () {
    $scope.show_si_no_reporte = false;
    $scope.show_si_reporte = true;
  }

  $scope.btnEnviarReporteSi = function (item) {
    item.serie = $scope.rSerie;
    item.id_reporte = $scope.rId_reporte;
    //console.log(item);
    TecnicoService.finishReportOk(item)
    .then(function(result){
      if (result.data.changedRows > 0) {
        $scope.show_si_reporte = false;
        $scope.show_si_reporte_ok = true;
      } else {
        alert("Hubo un error");
      }
      //console.log(result);
    });
  }

  //PARA BUSCAR REPORTE O SERIE MAQUINA
  $scope.show_search_by_serie = false;
  $scope.show_search_by_reporte = false;
  $scope.radioOpenSearchSerie = function () {
    $scope.show_search_by_serie = true;
    $scope.show_search_by_reporte = false;
    /*TecnicoService.getReporteBusqueda(item)
    .then(function(result){
      console.log(result);
    });*/
  }

  $scope.radioOpenSearchReporte = function (item) {
    $scope.show_search_by_serie = false;
    $scope.show_search_by_reporte = true;
  }
$scope.show_busqueda_serie_reporte = true;
$scope.show_busqueda_serie_reporte_ok = false;
$scope.show_reporte_no_atendido = false;
$scope.show_reporte_si_atendido = false;
$scope.show_alert_0_registros = false;
  $scope.buttonOpenSearchReporte = function (item) {
    TecnicoService.getReporteSearh(item)
    .then(function(result){
        if (result.data.err == true) {
          alert('Error al hacer la consulta, si el problema sigue apareciendo contacte al area de sistemas');
        } else {
          if (result.data == 0) {
            $scope.show_alert_0_registros = true;
          } else {
            console.log(result);
            $scope.show_busqueda_serie_reporte_ok = true;
            $scope.show_busqueda_serie_reporte = false;
            var d = result.data[0];
            $scope.bId_reporte = d.id_reporte;
            $scope.bId_maquina = d.id_maquina;
            $scope.bFecha_captura = d.fecha_captura;
            $scope.bCliente_captura = d.cliente_captura;
            $scope.bFalla_reportada = d.falla_reportada;
            if (d.fecha_atencion == null ) {
              $scope.show_reporte_no_atendido = true;
              $scope.show_reporte_si_atendido = false;
            } else {
              $scope.show_reporte_no_atendido = false;
              $scope.show_reporte_si_atendido = true;
              $scope.bFecha_atencion = d.fecha_atencion;
              $scope.bTecnico = d.tecnico_atiende;
              $scope.bTrabajo_efectuado = d.trabajo_efectuado;
              $scope.bNota = d.nota_motivo;
            }
          }

        }
    });
  }

  $scope.btnBuscarSerieReporte = function () {
    $scope.show_busqueda_serie_reporte = true;
    $scope.show_busqueda_serie_reporte_ok = false;
    $scope.show_reporte_no_atendido = false;
    $scope.show_reporte_si_atendido = false;
    $scope.show_search_by_serie = false;
    $scope.show_search_by_reporte = false;
    $scope.show_alert_0_registros = false;
  }



}]);
