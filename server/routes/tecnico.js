var express = require('express');
var router = express.Router();
var tecnicoCtrl = require('../controllers/tecnico');
var json2xls  = require("json2xls");

router.get('/get-mis-maquinas', function(req, res){
  var d = {tecnico: req.session.id_tecnico}
  tecnicoCtrl.getMisMaquinas(d)
  .then(function(result){
    res.json(result);
  });
});

router.get('/get-on-reports', function(req, res){
  var d = {tecnico: req.session.id_tecnico}
  tecnicoCtrl.getOnReports(d)
  .then(function(result){
    res.json(result);
  });
});

router.post('/finish-report-ok', function(req, res){
  var d = req.body;
  d.tecnico = req.session.id_tecnico;
  //var d = {tecnico: req.session.id_tecnico}
  tecnicoCtrl.finishReportOk(d)
  .then(function(result){
    res.json(result);
  });
});

router.post('/get-report-search', function(req, res){
  var d = req.body;
  console.log(d,'aqui get report');
  tecnicoCtrl.getReporteSearh(d)
  .then(function(result){
    res.json(result);
    console.log(result,'aqui resolve');
  });
});

module.exports = router;
