var express = require('express');
var router = express.Router();
var maquinaCtrl = require('../controllers/maquina');
var json2xls  = require("json2xls");

router.get('/get-maquina', function(req, res){

  maquinaCtrl.getMaquina()
  .then(function(result){
    //console.log(result);
    res.json(result);
  });
});

router.post('/do-report', function(req, res){
  var d = req.body;
  maquinaCtrl.doReport(d)
  .then(function(result){

    res.json(result);
  });
});

router.post('/get-bitacora', function(req, res){
  var d = req.body;
  maquinaCtrl.getBitacora(d)
  .then(function(result){
    console.log(result,'aqui controlador');
    res.json(result);
  });
});

module.exports = router;
