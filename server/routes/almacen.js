var express = require('express');
var router = express.Router();
var almacenCtrl = require('../controllers/almacen');


router.post('/search-serie', function(req, res){
  var d = req.body;
  almacenCtrl.searchSerie(d)
  .then(function(result){
    //console.log(result);
    res.json(result);
  });
});

router.post('/insert-machine', function(req, res){
  var d = req.body;
  almacenCtrl.insertMachine(d)
  .then(function(result){
    //console.log(result);
    res.json(result);
  });
});

router.post('/buscar-maquina', function(req, res){
  var d = req.body;
  almacenCtrl.buscarMaquina(d)
  .then(function(result){
    //console.log(result);
    res.json(result);
  });
});

router.post('/update-maquina', function(req, res){
  var d = req.body;
  almacenCtrl.updateInfoMaquina(d)
  .then(function(result){
    //console.log(result);
    res.json(result);
  });
});

router.get('/get-maquina', function(req, res){
  almacenCtrl.getMaquinas()
  .then(function(result){
    //console.log(result);
    res.json(result);
  });
});



module.exports = router;
