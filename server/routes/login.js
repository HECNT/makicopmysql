var express = require('express');
var router = express.Router();
var loginCtrl = require('../controllers/login');


router.post('', function(req, res){
  var login = req.body;
  console.log(login);
  loginCtrl.iniciar(login)
  .then(function(result){
    console.log(result,'routes');

          if(result.results.length > 0){
            req.session.logged= true;
            req.session.id_perfil = result.results[0].id_perfil;
            req.session.nombre_completo = result.results[0].nombre_completo;
            req.session.perfil = result.results[0].perfil;
            req.session.id_tecnico = result.results[0].id_tecnico;
            if (result.results[0].id_perfil == 1 ){
              result.results[0].url = '/makicop';
            }
            if (result.results[0].id_perfil == 2 ){
              //result[0].url = '/almacen_menu';
              result.results[0].url = '/almacen_menu';
            }
            if (result.results[0].id_perfil == 3 ){
              //result[0].url = '/almacen_menu';
              result.results[0].url = '/tecnico_menu';
            }
            res.json(result.results[0]);
            console.log(result.results[0]);
          }
      else{
        console.log('no');
      res.json({err:true, description: "Usuario no valido"});
      }


  });
});

router.put('', function(req, res){
  loginCtrl.salir()
  .then(function(result){
    console.log('aqui salir route');
    if(!result.err){
      req.session.logged = false;
      req.session.nombre = null;
    }
    res.json(result);
    console.log(result);
  });
});

router.post('/get-users', function(req, res){
  loginCtrl.getUsers()
  .then(function(result){
    res.json(result);
  });
});

router.get('/get-maquina', function(req, res){
  var d = {usr: req.session.id_perfil}
  console.log(d,'aqui req.session');
  loginCtrl.getMaquina(d)
  .then(function(result){
    console.log(result,'aquiiiiiii');
    res.json(result);

  });
});

module.exports = router;
