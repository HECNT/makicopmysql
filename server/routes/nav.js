var express = require('express');
var router = express.Router();

//Login
router.get('/', function(req,res){
  if(req.session.logged){
    if(req.session.id_perfil == 1){
  		res.redirect('makicop', {logged: req.session.logged, id: req.session.id_perfil, nombre_c: req.session.nombre_completo, perfil: req.session.perfil, id_tecnico: req.session.id_tecnico});
  	}
  }else{
  		res.render('login');
  }

});

router.get('/makicop', function(req,res){
  if(req.session.logged){
    if(req.session.id_perfil == 1){
  		res.render('menu', {logged: req.session.logged, id: req.session.id_perfil, nombre_c: req.session.nombre_completo, perfil: req.session.perfil, id_tecnico: req.session.id_tecnico});
  	}
  }else{
  		res.redirect('login');
  }

});

router.get('/almacen_menu', function(req,res){
  if(req.session.logged){
    if(req.session.id_perfil == 2){
  		res.render('almacen', {logged: req.session.logged, id: req.session.id_perfil, nombre_c: req.session.nombre_completo, perfil: req.session.perfil, id_tecnico: req.session.id_tecnico});
  	}
  }else{
  		res.redirect('login');
  }

});

router.get('/login', function(req,res){
  if(req.session.logged){
    if(req.session.id_perfil == 1){
  		res.redirect('makicop', {logged: req.session.logged, nombre: req.session.usr, nombre_c: req.session.nombre_completo, perfil: req.session.perfil, id_tecnico: req.session.id_tecnico});
  	}
  }else{
  		res.render('login');
  }

});

router.get('/login', function(req,res){
  if(req.session.logged){
    if(req.session.id_perfil == 2){
  		res.redirect('/almacen_menu', {logged: req.session.logged, nombre: req.session.usr, nombre_c: req.session.nombre_completo, perfil: req.session.perfil, id_tecnico: req.session.id_tecnico});
  	}
  }else{
  		res.render('login');
  }

});
//ROUTE PARA TECNCIO
router.get('/tecnico_menu', function(req,res){
  if(req.session.logged){
    if(req.session.id_perfil == 3){
  		res.render('tecnico', {logged: req.session.logged, id: req.session.id_perfil, nombre_c: req.session.nombre_completo, perfil: req.session.perfil, id_tecnico: req.session.id_tecnico});
  	}
  }else{
  		res.redirect('login');
  }

});

router.get('/login', function(req,res){
  if(req.session.logged){
    if(req.session.id_perfil == 3){
  		res.redirect('/tecnico_menu', {logged: req.session.logged, nombre: req.session.usr, nombre_c: req.session.nombre_completo, perfil: req.session.perfil, id_tecnico: req.session.id_tecnico});
  	}
  }else{
  		res.render('login');
  }

});

//*******************************************************************************************
module.exports = router;
