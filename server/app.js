var express = require("express");
var app = express();
var path = require("path");
var bodyParser = require("body-parser");
var cookieParser = require('cookie-parser');
var expressSession = require('express-session');

app.use(bodyParser.json());
app.use(express.static(path.join(__dirname,'..', 'public')));
app.use(bodyParser());

//Agregar sesion

app.use(cookieParser());
app.use(expressSession({secret: '78fhwe78hw78hdw78eh78w'}));

//Configuracion handlebar
var exphbs = require('express-handlebars');

app.engine('.hbs', exphbs({
  layoutsDir: path.join(__dirname,'..','client','views', 'layouts'),
  defaultLayout: 'main',
  extname: 'hbs'
}));

app.set('view engine', '.hbs');
app.set('views', path.join(__dirname,'..','client','views'));

//Ruta nav

var navRouter = require('./routes/nav');
app.use('/', navRouter);

//Ruta Login

var loginRouter = require("./routes/login");
app.use("/login", loginRouter);

//Ruta Maquina
var maquinaRouter = require("./routes/maquina");
app.use("/maquina", maquinaRouter);

//Ruta Almacen
var almacenRouter = require("./routes/almacen");
app.use("/almacen", almacenRouter);

//Ruta Tecnico
var tecnicoRouter = require("./routes/tecnico");
app.use("/tecnico", tecnicoRouter);

/*var prospectosRouter = require("./routes/prospectos");
app.use("/prospectos", prospectosRouter);*/

//Ruta fuentes
/*var fuentesRouter = require("./routes/fuentes");
app.use("/fuentes", fuentesRouter);

//Ruta detalles
var detallesRouter = require("./routes/detalles");
app.use("/detalles", detallesRouter);
*/

//var main = require("./models/main");
//main.start()
//.then(function(){
  app.listen(3000, function(){
    console.log("Escuchando en el puerto 3000");
  });
//});
