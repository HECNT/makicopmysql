var mysql      = require('mysql');
var connection = require('./main');


module.exports.getMisMaquinas = function(d){
  return new Promise(function(resolve, reject){
    connection.query(`
          SELECT
            *
          FROM
            maquinas
          WHERE
            tecnico = ?
          AND
            estatus = 1
      `,[d.tecnico], function(err, results) {
        if (err) {
          console.log(err);
          resolve({err:true, description: err});
        } else {
          resolve({err: false, results: results});
          //connection.end();
        }
    });
  })
}

module.exports.getOnReports = function(d){
  return new Promise(function(resolve, reject){
    connection.query(`
          SELECT
            *
          FROM
            maquinas as mq
          INNER JOIN
            reportes as rp
          ON
            mq.id_maquina = rp.id_maquina
          WHERE
            mq.tecnico = ?
          AND
            rp.estatus = 1
      `,[d.tecnico], function(err, results) {
        if (err) {
          console.log(err);
          resolve({err:true, description: err});
        } else {
          resolve({err: false, results: results});
          //connection.end();
        }
    });
  })
}

module.exports.finishReportOk = function(d){
  return new Promise(function(resolve, reject){
    connection.query(`
          UPDATE
            reportes
          SET
            fecha_atencion = now(),
            tecnico_atiende = ?,
            trabajo_efectuado = ?,
            estatus = 0,
            nota_motivo = ?
          WHERE
            id_reporte = ?
      `,[d.tecnico, d.trabajo_efectuado, d.nota, d.id_reporte], function(err, results) {
        if (err) {
          console.log(err);
          resolve({err:true, description: err});
        } else {
          resolve({err: false, results: results});
          //connection.end();
        }
    });
  })
}

module.exports.doLogSiReport = function(d){
  return new Promise(function(resolve, reject){
    connection.query(`
          INSERT INTO
            bitacora (id_maquina , accion, fecha_accion, responsable_accion)
          VALUES
            (?,'Finalizo_Reporte',now(), ?)
      `,[d.serie, d.tecnico], function(err, results) {
        if (err) {
          console.log(err);
          resolve({err:true, description: err});
        } else {
          resolve({err: false, results: results});
          //connection.end();
        }
    });
  })
}

module.exports.getReporteSearh = function(d){
  return new Promise(function(resolve, reject){
    connection.query(`
          SELECT
            *
          FROM
            reportes
          WHERE
            id_reporte = ?
      `,[d.id_reporte], function(err, results) {
        if (err) {
          console.log(err);
          resolve({err:true, description: err});
        } else {
          resolve({err: false, results: results});
          //connection.end();
        }
    });
  })
}
