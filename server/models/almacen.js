var mysql      = require('mysql');
var connection = require('./main');

module.exports.searchSerie = function(d){
  console.log(d,'almacen Model');
  return new Promise(function(resolve, reject){
    connection.query(`
          SELECT
            *
          FROM
            maquinas
          WHERE
            serie = ?
      `,[d.serie], function(err, results) {
        if (err) {
          console.log(err);
          resolve({err:true, description: err});
        } else {
          resolve({err: false, results: results});
          //connection.end();
        }
    });
  })
}

module.exports.insertMachine = function(d){
  console.log(d,'almacen Model');
  return new Promise(function(resolve, reject){
    connection.query(`
          INSERT INTO
            maquinas (serie,   modelo,   marca, contador_negro,contador_color,agregados,cliente,tecnico,calle,   colonia,municipio,estado,     pais,   cp,contacto,telefono,departamento,sub_cliente,estatus,fecha_alta)
          VALUES
            (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,1,now())
      `,[d.serie,d.modelo,d.marca,d.negro,d.color,d.agregados,d.cliente,d.tecnico,d.calle,d.colonia,d.municipio,d.estado,d.pais,d.cp,d.contacto,d.telefono,d.departamento,d.subcliente], function(err, results) {
        if (err) {
          console.log(err);
          resolve({err:true, description: err});
        } else {
          resolve({err: false, results: results});
          //connection.end();
        }
    });
  })
}

module.exports.buscarMaquina = function(d){
  console.log(d,'almacen Model');
  var bP = d.tipo == "id"     ? "id_maquina ="
         : d.tipo == "serie"  ? "serie ="
         : d.tipo == "modelo" ? "modelo ="
         : "";
  return new Promise(function(resolve, reject){
    connection.query(`
          SELECT
            *
          FROM
            maquinas
          WHERE
            ${bP} ?
      `,[d.por], function(err, results) {
        if (err) {
          console.log(err);
          resolve({err:true, description: err});
        } else {
          resolve({err: false, results: results});
          //connection.end();
        }
    });
  })
}

module.exports.updateInfoMaquina = function(d){
  console.log(d,'almacen Model');
  return new Promise(function(resolve, reject){
    connection.query(`
          UPDATE
            maquinas
          SET
            serie = ?,
            modelo = ?,
            marca = ?,
            contador_negro = ?,
            contador_color = ?,
            agregados = ?,
            cliente = ?,
            tecnico = ?,
            calle = ?,
            colonia = ?,
            municipio = ?,
            estado = ?,
            pais = ?,
            cp = ?,
            contacto = ?,
            telefono = ?,
            departamento = ?,
            sub_cliente = ?
          WHERE id_maquina = ?
      `,[d.bSerie,d.bModelo,d.bMarca,d.bNegro,d.bColor,d.bAgregados,d.bCliente,d.bTecnico,d.bCalle,d.bColonia,d.bMunicipio,d.bEstado,d.bPais,d.bCp,d.bContacto,d.bTelefono,d.bDepartamento,d.bSubCliente,d.bIdMaquina], function(err, results) {
        if (err) {
          console.log(err);
          resolve({err:true, description: err});
        } else {
          resolve({err: false, results: results});
          //connection.end();
        }
    });
  })
}

module.exports.getMaquinas = function(){
  return new Promise(function(resolve, reject){
    connection.query(`
          SELECT
            *
          FROM
            maquinas
          ORDER BY
            id_maquina
          DESC
      `, function(err, results) {
        if (err) {
          console.log(err);
          resolve({err:true, description: err});
        } else {
          resolve({err: false, results: results});
          //connection.end();
        }
    });
  })
}

module.exports.logInsertMaquina = function(d){
  return new Promise(function(resolve, reject){
    connection.query(`
          INSERT INTO
            bitacora (id_maquina, accion, fecha_accion,responsable_accion)
          VALUES
            (?,?,NOW(),1)
      `,[d.serie,d.accion] ,function(err, results) {
        if (err) {
          console.log(err);
          resolve({err:true, description: err});
        } else {
          resolve({err: false, results: results});
          //connection.end();
        }
    });
  })
}

module.exports.logUpdateMaquina = function(d){
  return new Promise(function(resolve, reject){
    connection.query(`
          INSERT INTO
            bitacora (id_maquina, accion, fecha_accion,responsable_accion)
          VALUES
            (?,?,NOW(),1)
      `,[d.bSerie,d.accion] ,function(err, results) {
        if (err) {
          console.log(err);
          resolve({err:true, description: err});
        } else {
          resolve({err: false, results: results});
          //connection.end();
        }
    });
  })
}
