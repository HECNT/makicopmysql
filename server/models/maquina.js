var mysql      = require('mysql');
var connection = require('./main');
//console.log(connection);
module.exports.getMaquina = function(){
  //connection.connect();
  return new Promise(function(resolve, reject){
    connection.query(`
              SELECT
                *
              FROM
                maquinas


      `, function(err, results) {
        if (err) {
          console.log(err);
          resolve({err:true, description: err});
        } else {
          resolve({err: false, results: results});

          //connection.end();
        }
    });
  })
}

module.exports.doReport = function(d){
  return new Promise(function(resolve, reject){
    connection.query(`
          INSERT INTO
            reportes (id_maquina,fecha_captura,cliente_captura,falla_reportada,estatus)
          VALUES
            (?,now(),?,?,1)
      `,[d.id_maquina,d.reporta,d.reporte_falla], function(err, results) {
        if (err) {
          console.log(err);
          resolve({err:true, description: err});
        } else {
          resolve({err: false, results: results});
          //connection.end();
        }
    });
  })
}

module.exports.doBitacora = function(d){
  return new Promise(function(resolve, reject){
    connection.query(`
            INSERT INTO
              bitacora (id_maquina, accion, fecha_accion,responsable_accion)
            VALUES
              (?,?,NOW(),1)
      `,[d.serie,d.accion], function(err, results) {
        if (err) {
          console.log(err);
          resolve({err:true, description: err});
        } else {
          resolve({err: false, results: results});
          //connection.end();
        }
    });
  })
}

module.exports.getBitacora = function(d){
  return new Promise(function(resolve, reject){
    connection.query(`
          SELECT
            *
          FROM
            maquinas as ma
          INNER JOIN
            bitacora as bi
          ON
            ma.serie = bi.id_maquina
          WHERE
            ma.id_maquina = ?
      `,[d.id_maquina], function(err, results) {
        if (err) {
          console.log(err);
          resolve({err:true, description: err});
        } else {
          resolve({err: false, results: results});
          //connection.end();
        }
    });
  })
}
