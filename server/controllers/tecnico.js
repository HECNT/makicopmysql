var Tecnico = require('../models/tecnico');
var nodemailer = require('nodemailer');

module.exports.getMisMaquinas = function(d){
  return new Promise(function(resolve, reject){
    Tecnico.getMisMaquinas(d)
    .then(function(result){
      resolve(!result.err ? result.results : result);
    });
  });
}

module.exports.getOnReports = function(d){
  return new Promise(function(resolve, reject){
    Tecnico.getOnReports(d)
    .then(function(result){
      resolve(!result.err ? result.results : result);
    });
  });
}

module.exports.finishReportOk = function(d){
  return new Promise(function(resolve, reject){
    Tecnico.finishReportOk(d)
    .then(function(result){

      if (result.err == false) {
        Tecnico.doLogSiReport(d)
        .then(function(result){
          console.log('Se genero log finalizar reporte');
        });
        resolve(!result.err ? result.results : result);
      } else {
        resolve(!result.err ? result.results : 'Error no se pudo realizar la consulta');
      }

    });
  });
}

module.exports.getReporteSearh = function(d){
  return new Promise(function(resolve, reject){
    Tecnico.getReporteSearh(d)
    .then(function(result){
      resolve(!result.err ? result.results : result);
    });
  });
}
