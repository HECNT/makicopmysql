var LoginModel = require('../models/maquina');
var nodemailer = require('nodemailer');

module.exports.getMaquina = function(){

  return new Promise(function(resolve, reject){
    LoginModel.getMaquina()
    .then(function(result){
      //console.log(result,'maquin');
      resolve(!result.err ? result.results : result);
    });
  });
}

module.exports.doReport = function(d){
  return new Promise(function(resolve, reject){
    LoginModel.doReport(d)
    .then(function(result){
      if (result.err == false) {


        // create reusable transporter object using the default SMTP transport
        var transporter = nodemailer.createTransport('smtps://reporteshectn%40gmail.com:soldadohalo3ods@smtp.gmail.com');

        // setup e-mail data with unicode symbols
        var mailOptions = {
            from: '"Reporte Makicop" <reporteshectn@gmail.com>', // sender address
            to: 'undercoverosva@outlook.com, mhernandez.makicop@gmail.com, jhernandez.makicop@gmail.com', // list of receivers
            subject: 'Notificacion Reporte ✔', // Subject line
            text: 'Prueba', // plaintext body
            html: '<h1>Falla:</h1>'+ d.reporte_falla + '<h1> Quien reporta: </h1>'+ d.reporta
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, function(error, info){
            if(error){
                return console.log(error);
            }
            console.log('Message sent: ' + info.response);
        });
        resolve(!result.err ? result.results : result);
      } else {
        console.log('err fallo algo');
      }

    });
    console.log(d,'aqui maquinassssssss');
    d.accion = 'Se genero reporte';
    LoginModel.doBitacora(d)
    .then(function(result){
      if (result.err == false) {
        console.log('bitacora hecho');
      } else {
        console.log('err bitacora');
      }
    });
  });
}

module.exports.getBitacora = function(d){

  return new Promise(function(resolve, reject){
    LoginModel.getBitacora(d)
    .then(function(result){
      console.log(result);
      resolve(!result.err ? result.results : result);
    });
  });
}
