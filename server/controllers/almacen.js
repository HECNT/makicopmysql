var Almacen = require('../models/almacen');

module.exports.searchSerie = function(d){
  return new Promise(function(resolve, reject){
    Almacen.searchSerie(d)
    .then(function(result){
      resolve(!result.err ? result.results : result);
    });
  });
}

module.exports.insertMachine = function(d){
  return new Promise(function(resolve, reject){
    Almacen.insertMachine(d)
    .then(function(result){
      resolve(!result.err ? result.results : result);
    });
    d.accion = "Agregar_maquina";
    Almacen.logInsertMaquina(d)
    .then(function(result){
      resolve(!result.err ? result.results : result);
    });
  });
}

module.exports.buscarMaquina = function(d){
  return new Promise(function(resolve, reject){
    Almacen.buscarMaquina(d)
    .then(function(result){
      resolve(!result.err ? result.results : result);
    });
  });
}

module.exports.updateInfoMaquina = function(d){
  return new Promise(function(resolve, reject){
    Almacen.updateInfoMaquina(d)
    .then(function(result){
      resolve(!result.err ? result.results : result);
    });
    console.log(d,'aqui');
    d.accion = "Se modificaron campos";
    Almacen.logUpdateMaquina(d)
    .then(function(result){
      if (result.err == false) {
        console.log('Se genero log');
      } else {
        console.log('Hubo problema al generar log');
      }
    });
  });
}

module.exports.getMaquinas = function(){
  return new Promise(function(resolve, reject){
    Almacen.getMaquinas()
    .then(function(result){
      resolve(!result.err ? result.results : result);
    });
  });
}
